"use strict";

function Bomb() {
    var self = this;
    self.posY = 0;
    self.speedY = 5;
    self.domBomb = document.createElement('div');
    self.domBomb.className = "bomb";
    container.appendChild(self.domBomb);
    self.update = function() {
        self.domBomb.style.left = Math.round(self.posX) + "px";
        self.domBomb.style.top = Math.round(self.posY) + "px";

    };
    self.getCoordsBomb = function () {
        var coords  = self.domBomb.getBoundingClientRect();
        return {
            xmin: coords.left,
            xmax: coords.right,
            ymin: coords.top,
            ymax: coords.bottom
        }
    };
    self.getRandom = function (min, max){
        return Math.random() * (max - min) + min;
    };
    self.posX = self.getRandom (270,700);
    self.returnPosXPosY = function(){
        return {posX: self.posX, posY: self.posY};
    };
    self.stopBomb = function() {
            self.domBomb.style.visibility = 'hidden';
            self.posX = self.getRandom (270,700);
            self.posY = 0;
    };
    self.moveBomb = function(EO) {
        EO = EO || window.event;
        self.domBomb.style.visibility = 'visible';
        if(self.posY >= 0 && self.posY < 540  ){
            self.posY += self.speedY;
        } else if (self.posY >= 540){
            self.stopBomb();
        }
        self.update();
    }
}

