debugger
$(document).ready(function() {
    select_partial();

    $(window).bind('hashchange', function() {
        select_partial();
    });
});

function select_partial() {
    var url = window.location.href;
    var hash = url.substring(url.indexOf("#")+1);
    switch (hash) {
        case 'main':
            $(".wrapper").load("main.html");
            break;
        case 'game':
            $(".wrapper").load("game.html");
            break;
        case 'records':
            $(".wrapper").load("records.html");
            break;
        default:
            var url = document.location.href.match(/(^[^#]*)/)[0];
            $(location).attr('href', url + '#main');
    }
}

