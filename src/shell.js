function Shell() {
    var self = this;
    self.posX = 0;
    self.posY = 0;
    self.is_run = false;
    self.speedY = 10;
    self.domShell = document.createElement('div');
    self.domShell.className = "shell";
    container.appendChild(self.domShell);
    self.update = function() {
        self.domShell.style.left = Math.round(self.posX) + 9 + "px";//9 поправка, чтобы заряд вылетал из середины игрока (половина ширины player)
        self.domShell.style.top = Math.round(self.posY) + "px";

    };
    self.refrash = function() {
        self.posX = 0;
        self.posY = 0;
        self.is_run = false;
        self.domShell.style.visibility = 'hidden';
        self.update();
    };
    self.returnPosXPosY = function(){
        return {posX: self.posX, posY: self.posY};
    };
    self.getCoordsShell = function () {
        var coords  = self.domShell.getBoundingClientRect();
        return {
            xmin: coords.left,
            xmax: coords.right,
            ymin: coords.top,
            ymax: coords.bottom
        }
    };
    self.startShell = function(posX, posY) {
        self.posX = posX;
        self.posY = posY;
        self.domShell.style.visibility = 'visible';
        self.is_run = true;
    };
    self.goShell = function () {
        if (self.posY > 0) {
            self.posY -= self.speedY;
        } else if (self.posY <= 0){
            self.stopShell();
        }
        self.update();
    };
    self.stopShell = function() {
            self.domShell.style.visibility = 'hidden';
            self.posX = 0;
            self.posY = 0;
            self.update();
            clearInterval(timerIdShell);
            self.is_run = false;
    };
    self.moveShell = function(posX, posY) {
        if( posX === undefined) {
            posX = self.posX
        }
        if( posY === undefined) {
            posY = self.posY
        }

        if (!self.is_run) {
            self.startShell(posX, posY);
            soundMovementShell();
            timerIdShell = setInterval(self.goShell, 40)
        }
    }
}
