"use strict";

function Player(playerName) {
    var self = this;
    self.name = playerName;
    self.posX = 480;
    self.posY = 500; //середина поля
    self.speedX = 0;
    self.destroy = false;
    self.lifeCount = document.getElementById('lifeCount');
    self.lifeCount.innerHTML = 3;
    self.getWidth = function() {
        if(document.getElementById('player')) {
            return document.getElementById('player').offsetWidth;
        } else {
            return 0;
        }
    };
    self.elemPlayer = document.getElementById('player');
    self.update = function() {
        self.elemPlayer.style.left = Math.round(self.posX) + "px";
        self.elemPlayer.style.top = Math.round(self.posY) + "px";
    };
    self.returnPosXPosY = function(){
        return {posX: self.posX, posY: self.posY, name: self.name};
    };
    self.getCoordsElemPlayer = function () {
        var coords  = self.elemPlayer.getBoundingClientRect();
        return {
            xmin: coords.left,
            xmax: coords.right,
            ymin: coords.top,
            ymax: coords.bottom
        }
    };
    self.reduceLives = function() {
        if (self.lifeCount.innerHTML > 0){
            self.lifeCount.innerHTML --;
        } else if (self.lifeCount.innerHTML == 0){
            self.lifeCount.innerHTML = "";
        }

    };
    self.startPlayer = function(EO){
        EO=EO||window.event;
        switch(EO.keyCode){
            case 39:   // если нажата клавиша вправо
                self.speedX = 2;
                break;
            case 37:   // если нажата клавиша влево
                self.speedX = -2;
                break;
            case 32:   // если нажата клавиша пробел
                if(game_is_start === true) {
                    shell.moveShell(self.posX, self.posY);
                }
                break;
        }

    };
    self.stopPlayer = function(EO) {
        EO = EO || window.event;
        if ([39, 37].includes(EO.keyCode)) {
            self.speedX = 0;
        }
    };
    self.movePlayer = function(EO) {
        EO = EO || window.event;
        self.posX += self.speedX;
        //вышел ли player за правые пределы поля?
        if(self.posX > 700 - self.getWidth()){// 700 ширина поля
            self.posX = 700 - self.getWidth();
        }
        //вышел ли player за левые пределы поля?
        if(self.posX < 270){
            self.posX = 270;
        }
        self.update();
    }

    self.saveRecord = function(score) {
        var ajax = new AjaxClass();
        ajax.userName = self.name;
        ajax.myValue = score;
        ajax.getRecords();
    }
}