"use strict";

function Enemy(enemyName, className) {
    var self = this;
    self.name = enemyName;
    self.posX = 180;
    self.posY = 140;
    self.speedY = 20;
    self.speedX = 24;
    self.is_delete = false;
    self.scoreCount = document.getElementById('scoreCount');
    self.className = className;
    self.elemEnemy = document.createElement('div');
    container.appendChild(self.elemEnemy);
    self.elemEnemy.className = self.className;
    self.update = function () {
        self.elemEnemy.style.left = Math.round(self.posX) + "px";
        self.elemEnemy.style.top = Math.round(self.posY) + "px";
        self.elemEnemy.style.right = Math.round(self.posX) + self.elemEnemy.offsetWidth + "px";
        self.elemEnemy.style.bottom = Math.round(self.posY) + self.elemEnemy.offsetHeight + "px";
    };
    self.returnPosXPosY = function(){
        return {posX: self.posX, posY: self.posY, className: self.className, is_delete: self.is_delete};
    };
    self.getCoordsElemEnemy = function () {
        var coords  = self.elemEnemy.getBoundingClientRect();
        return {
            xmin: coords.left,
            xmax: coords.right,
            ymin: coords.top,
            ymax: coords.bottom
        }
    };
    self.increaseScore = function(item) {
        var count = +self.scoreCount.innerHTML;
        if(item.className == 'enemyGreen'){
            count += 20;
        } else if(item.className == 'enemyRose'){
            count += 40;
        }else if(item.className == 'enemyBlue'){
            count += 60;
        }
        self.scoreCount.innerHTML = count;
    };
    self.moveEnemy = function (EO) {
        EO = EO || window.event;
        //дошли ли враги до player
        if (self.posY == 500){
            clearInterval(timerId);
            clearInterval(timerIdIntersectionBombAndPlayer);
            clearInterval(timerIdBomb);
            clearInterval(timerIdPlayer);
            clearInterval(timerIdIntersectionShellAndEnemy);
            gameOver()
        }
        //вышел ли enemy за пределы поля справа и слева?
        if (count == 9) {
            self.posY = self.posY + self.speedY;
        } else if (count >= 9 && count < 19) {
            self.posX += -self.speedX
        } else if (count < 9) {
            self.posX += self.speedX;
        } else if (count == 19) {
            self.posY = self.posY + self.speedY;
        } else if (count > 19) {
            count = 0;
            self.posX += self.speedX;
        }
        self.update();
    }
}



