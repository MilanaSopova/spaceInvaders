saveData = function (){
  pauseGame();
  var dataShell = shell.returnPosXPosY();
  var dataPlayer = player.returnPosXPosY();
  var dataBomb = bomb.returnPosXPosY();
  var dataEnemy = arrayEnemy.map(function(item){
    return item.returnPosXPosY();
  });
  var scoreCount = document.getElementById('scoreCount');
  var dataScoreCount = scoreCount.innerHTML;
  var dataPlayerLifeCount = player.lifeCount.innerHTML;
  var saveParams = {dataShell: dataShell, dataPlayer: dataPlayer, dataBomb: dataBomb, dataEnemy: dataEnemy, dataScoreCount: dataScoreCount, dataPlayerLifeCount: dataPlayerLifeCount};
  localStorage.setItem('save_game', JSON.stringify(saveParams));
  alert('Play was saved!')
};

var loadData = function() {
  if(localStorage.getItem('save_game')) {
    var saveParams = JSON.parse(localStorage.getItem('save_game'));

    player.posX = saveParams['dataPlayer'].posX;
    player.posY = saveParams['dataPlayer'].posY;
    player.name = saveParams['dataPlayer'].name;

    shell.posX = saveParams['dataShell'].posX;
    shell.posY = saveParams['dataShell'].posY;

    bomb.posX = saveParams['dataBomb'].posX;
    bomb.posY = saveParams['dataBomb'].posY;

    document.getElementById('scoreCount').innerHTML = saveParams['dataScoreCount'];

    arrayEnemy.forEach(function(item){
      item.elemEnemy.remove();
    });
    arrayEnemy = [];

    player.lifeCount.innerHTML = saveParams['dataPlayerLifeCount'];
    var dataEnemy = saveParams['dataEnemy'];
    var dataEnemy = dataEnemy.forEach(function(item){
      if(!item.is_delete) {
        if (item.className == 'enemyBlue') {
          var enemy = new Enemy('enemyBlue', 'enemyBlue')
        } else if (item.className == 'enemyRose') {
          var enemy = new Enemy('enemyRose', 'enemyRose');
        } else if (item.className == 'enemyGreen') {
          var enemy = new Enemy('enemyGreen', 'enemyGreen');
        }

        arrayEnemy.push(enemy);
        enemy.posX = item.posX;
        enemy.posY = item.posY;
        enemy.update();
      }
    });
    playGame();
  } else {
    console.log('Error!')
  }
}