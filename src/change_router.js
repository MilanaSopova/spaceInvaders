$(document).ready(function() {
    $("#game_button").on( "click", function() {
        var url = document.location.href.match(/(^[^#]*)/)[0];
        $(location).attr('href', url + '#game');
    });

    $("#main_button").on( "click", function() {
        var url = document.location.href.match(/(^[^#]*)/)[0];
        $(location).attr('href', url + '#main');
    });

    $("#records").on( "click", function() {
        var url = document.location.href.match(/(^[^#]*)/)[0];
        $(location).attr('href', url + '#records');
    });
});