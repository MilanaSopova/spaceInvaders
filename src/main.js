"use strict";
var container = document.getElementById('container');// поле , в котором происходит игра
var player = new Player('player1');
var z = 0;//расстояние сдвига каждого врага
var x = 0;//смещение каждого последующего врага
var y = 0;
var w = 0;
var arrayEnemy = [];
var count = 0;// счетчик прыжков врагов
var timerId = undefined;
var shell = new Shell();
var bomb = new Bomb();
var timerIdBomb = undefined;
var timerIdPlayer = undefined;
var timerIdIntersectionBombAndPlayer = undefined;
var timerIdIntersectionShellAndEnemy = undefined;
var timerIdShell = undefined;
var game_is_start = false;
var name = undefined;

player.update();

function createEnemy(){
    for( var i = 1;i < 12;i ++){
        var enemyBlue = new Enemy('enemyBlue','enemyBlue');
        arrayEnemy.push(enemyBlue);
        x += 30;
        enemyBlue.posX = enemyBlue.posX + x;//4 это поправка на ширину розового врага, так как он уже всех
        enemyBlue.posY = enemyBlue.posY + 20;
        enemyBlue.update();
    }
    for( var a = 1;a < 12;a ++){
        var enemyRose = new Enemy('enemyRose', 'enemyRose');
        arrayEnemy.push(enemyRose);
        z += 30;
        enemyRose.posX = enemyRose.posX + 5 + z;//5 это поправка на ширину розового врага, так как он уже всех
        enemyRose.posY = enemyRose.posY + 40;
        enemyRose.update();
    }
    for( var b = 1;b < 12;b ++){
        var enemyGreen = new Enemy('enemyGreen', 'enemyGreen');
        arrayEnemy.push(enemyGreen);
        y += 30;
        enemyGreen.posX = enemyGreen.posX + y;
        enemyGreen.posY = enemyGreen.posY + 60;
        enemyGreen.update();
    }
    for( var c = 1;c < 12;c ++){
        enemyGreen = new Enemy('enemyGreen', 'enemyGreen');
        arrayEnemy.push(enemyGreen);
        w += 30;
        enemyGreen.posX = enemyGreen.posX + w;
        enemyGreen.posY = enemyGreen.posY + 80;
        enemyGreen.update();
    }
}

createEnemy();

function moveAllEnemy() {
    soundMovementEnemy();
    arrayEnemy.forEach(function(item) {
        item.moveEnemy()
    });
    count++;
}

function intersectionBombAndPlayer() {//функция расчета столкновения бомбы и плэера и потери жизней плеера
    var coordsBomb = this.bomb.getCoordsBomb();
    var coordsPlayer = this.player.getCoordsElemPlayer();
    var e = (coordsBomb.xmax - coordsBomb.xmin + coordsPlayer.xmax - coordsPlayer.xmin
            - Math.abs(coordsBomb.xmax + coordsBomb.xmin - coordsPlayer.xmax - coordsPlayer.xmin)) / 2,
        f = (coordsBomb.ymax - coordsBomb.ymin + coordsPlayer.ymax - coordsPlayer.ymin
            - Math.abs(coordsBomb.ymax + coordsBomb.ymin - coordsPlayer.ymax - coordsPlayer.ymin)) / 2;
    if (e > 0 && f > 0) {
        player.reduceLives();
        var elemExplosion = document.createElement('div');
        elemExplosion.className = "explosionPlayer";
        container.appendChild(elemExplosion);
        elemExplosion.style.left = this.player.posX + "px";
        elemExplosion.style.top = this.player.posY + "px";
        // this.player.changeDestroyStatus();
        soundIntersectionBombAndPlayer();
        var timerPlayerExplosion = setTimeout(resumePlayer, 1000);
        this.bomb.stopBomb();
        clearInterval(timerIdPlayer);
    }
    if (player.lifeCount.innerHTML == ""){
        clearInterval(timerId);
        clearInterval(timerIdIntersectionBombAndPlayer);
        clearInterval(timerIdBomb);
        clearInterval(timerIdPlayer);
        clearInterval(timerIdIntersectionShellAndEnemy);
        gameOver()
    }
    // clearTimeout(timerPlayerExplosion);
}

function resumePlayer(){
    timerIdPlayer = setInterval(player.movePlayer,40);
}

function intersectionShellAndEnemy() {//функция расчета столкновения снаряда и врагов и зарабатывания очков
    var coordsShell = shell.getCoordsShell();
    arrayEnemy.forEach(function(item){
        var coordsEnemy = item.getCoordsElemEnemy();
        var elemExplosion = document.createElement('div');
        function makeExplosion(){
            var elemExplosion = document.createElement('div');
            elemExplosion.className = "explosion";
            container.appendChild(elemExplosion);
            elemExplosion.style.left = item.posX - 5 + "px";//поправка на центрирование взрыва
            elemExplosion.style.top = item.posY + "px";
        }
        var t = (coordsShell.xmax - coordsShell.xmin + coordsEnemy.xmax - coordsEnemy.xmin
                - Math.abs(coordsShell.xmax + coordsShell.xmin - coordsEnemy.xmax - coordsEnemy.xmin)) / 2,
            l = (coordsShell.ymax - coordsShell.ymin + coordsEnemy.ymax - coordsEnemy.ymin
                - Math.abs(coordsShell.ymax + coordsShell.ymin - coordsEnemy.ymax - coordsEnemy.ymin)) / 2;
        if (t > 0 && l > 0) {
            shell.stopShell();
            item.elemEnemy.remove();
            makeExplosion();
            soundIntersectionShellAndEnemy();
            item.increaseScore(item);
            item.is_delete = true;
            areYouWin()
        }
    });
}

function areYouWin() {
    self.allEnemyDestroy = true;
    $.each( arrayEnemy, function( key, value ) {
        if(value.is_delete == false){
            self.allEnemyDestroy = false
        }
    });
    if(self.allEnemyDestroy == true) {
        gameWin();
    }
}

function playNewGame(){ // начать игру заново
    pauseGame();
    //обнулить счет
    var scoreCount = document.getElementById('scoreCount');
    scoreCount.innerHTML = '0';
    player.lifeCount.innerHTML = 3;

    timerIdIntersectionBombAndPlayer = undefined;
    timerIdIntersectionShellAndEnemy = undefined;
    z = 0;//расстояние сдвига каждого врага
    x = 0;//смещение каждого последующего врага
    y = 0;
    w = 0;
    timerId = undefined;// в начальное положение вернулись враги
    count = 0;
    arrayEnemy.forEach(function(item){
        item.elemEnemy.remove();
    });
    arrayEnemy = [];
    createEnemy();
    timerId = setInterval(moveAllEnemy, 1000);

    timerIdBomb = undefined;// в начальное положение вернулся bomb
    bomb.posX = bomb.getRandom (270,700);
    bomb.posY = 0;
    timerIdBomb  = setInterval(bomb.moveBomb, 40);

    timerIdPlayer = undefined;// в начальное положение вернулся player
    player.posX = 480;
    player.posY = 500;
    timerIdPlayer = setInterval(player.movePlayer,40);

    timerIdShell = undefined;// в начальное положение вернулся shell
    game_is_start = true;
    shell.refrash();
    document.addEventListener("keydown",player.startPlayer);
    document.addEventListener("keyup",player.stopPlayer);
}

function playGame() {// продолжить игру
    pauseGame();
    if((timerId == undefined)||(arrayEnemy.is_move == false)){
        timerId = setInterval(moveAllEnemy, 1000);
        arrayEnemy.is_move =  true;
    }
    if((timerIdBomb  == undefined)||(bomb.is_move == false)){
        timerIdBomb  = setInterval(bomb.moveBomb, 40);
        bomb.is_move = true;
    }
    if((timerIdPlayer == undefined)||(player.is_move == false)){
        timerIdPlayer = setInterval(player.movePlayer,40);
        player.is_move =  true;
    }
    if((timerIdShell == undefined)||(shell.is_move == false)){
        var restartShell = function(){//заставляет снаряд снова двигаться
            if ( shell.is_run == false) {
                shell.moveShell();
            }
        };
        restartShell();
    }
    game_is_start = true;
    document.addEventListener("keydown",player.startPlayer);
    document.addEventListener("keyup",player.stopPlayer);
}

function pauseGame() {
    arrayEnemy.is_move = false;
    bomb.is_move = false;
    player.is_move = false;
    shell.is_move = false;
    if(bomb.is_move == false){
        clearInterval(timerIdBomb);
    }
    if(arrayEnemy.is_move == false){
        clearInterval(timerId);
    }
    if(player.is_move == false){
        clearInterval(timerIdPlayer);
    }
    clearInterval(timerIdShell);
    shell.is_run = false;
    game_is_start = false;
}

function soundIntersectionShellAndEnemy() {
    var audio = new Audio(); // Создаём новый элемент Audio
    audio.src = "music/invaderkilled.wav"; // Указываем путь к звуку "клика"
    audio.autoplay = true; // Автоматически запускаем
}

function soundIntersectionBombAndPlayer() {
    var audio = new Audio(); // Создаём новый элемент Audio
    audio.src = "music/explosion.wav"; // Указываем путь к звуку "клика"
    audio.autoplay = true; // Автоматически запускаем
}

function soundMovementShell() {
    var audio = new Audio(); // Создаём новый элемент Audio
    audio.src = "music/ufo_highpitch.wav"; // Указываем путь к звуку "клика"
    audio.autoplay = true; // Автоматически запускаем
}

function soundMovementEnemy() {
    var audio = new Audio(); // Создаём новый элемент Audio
    audio.src = "music/fastinvader1.wav"; // Указываем путь к звуку "клика"
    audio.autoplay = true; // Автоматически запускаем
}

function buttonPlay(){

    if(timerId === undefined){
        do {
            var name = prompt ('Введите ваше имя');
            player.name = name;
        } while (name === null || name.length == 0);

        var startNewGame = confirm("Начать новую игру?");
        if (startNewGame === true) {
            playGame();
        }
    } else {
        startNewGame = confirm("Продолжить игру?");
        if (startNewGame === true) {
            playGame();
        } else {
            startNewGame = confirm("Начать игру заново?");
            if (startNewGame === true) {
                playNewGame();
            } else {
                startNewGame = confirm("Выйти?");//доделать эту функцию
                if (startNewGame === true) {
                    var url = document.location.href.match(/(^[^#]*)/)[0];
                    $(location).attr('href', url + '#main');
                }
            }
        }
    }
}
$( "#play_game" ).on( "click", function() {
    this.blur();
    buttonPlay();
});

$( "#pause" ).on( "click", function() {
    this.blur();
    pauseGame();
});

$( "#save" ).on( "click", function() {
    this.blur();
    saveData();
});

$( "#load" ).on( "click", function() {
    this.blur();
    loadData();
});

function gameOver() {
    player.saveRecord(+$('#scoreCount').text());
    var info = $( ".info-message" );
    info.html("GAM<span>E</span> <span>O</span>VE<span>R</span>");
    info.show();
    setTimeout(redirectToRecords, 5000);
}

function gameWin() {
    player.saveRecord(+$('#scoreCount').text());
    var info = $( ".info-message" );
    info.html("W<span>I</span>N");
    info.show();
    setTimeout(redirectToRecords, 5000);
}

function redirectToRecords() {
    $( ".info-message" ).hide();
    var url = document.location.href.match(/(^[^#]*)/)[0];
    $(location).attr('href', url + '#records');
}

timerIdIntersectionBombAndPlayer = setInterval(intersectionBombAndPlayer.bind(this), 40);
timerIdIntersectionShellAndEnemy = setInterval(intersectionShellAndEnemy.bind(this), 40);






