"use strict";

$(function() {
  if($('.records-list').length > 0) {
    var ajax = new AjaxClass();
    ajax.readRecords();
  }
});

function AjaxClass() {
  var self = this;
  self.ajaxHandlerScript = "https://fe.it-academy.by/AjaxStringStorage2.php";
  self.updatePassword = undefined;
  self.stringName = 'spaceInvadersSopova';
  self.password = '123456789';
  self.new_value = 0;
  self.myValue = 0;
  self.userName = 'NaN';
  self.new_records = {};


  self.sendRecords = function (records) {
    $.ajax({
        url: self.ajaxHandlerScript,
        type: 'POST', dataType: 'json',
        data: {
          f: 'UPDATE', n: self.stringName,
          p: self.password, v: records
        },
        cache: false,
        success: saveCollBack,
        error: errorHandler
      }
    );
  };

  self.readRecords = function () {
    $.ajax({
        url: self.ajaxHandlerScript,
        type: 'POST', dataType: 'json',
        data: {f: 'READ', n: self.stringName},
        cache: false,
        success: readCollBack,
        error: errorHandler
      }
    );
  };

  self.getRecords = function () {
    $.ajax({
        url: self.ajaxHandlerScript,
        type: 'POST', dataType: 'json',
        data: {f: 'LOCKGET', n: self.stringName, p: self.password},
        cache: false,
        success: getCollBack,
        error: errorHandler
      }
    );
  };

  function getCollBack(callresult) {
    if (callresult.error != undefined)
      alert(callresult.error);
    else {
      var result = JSON.parse(callresult.result);
      if (!jQuery.isEmptyObject(result)) {
        if(Object.keys(result).length < 5){
          result[self.userName] = self.myValue
        } else {
          for (var hash_min_key in result) break;
          var min = result[hash_min_key];
          Object.keys(result).map(function(objectKey, index) {
            if(result[objectKey] < min){
              hash_min_key = objectKey
            }
          });
          if(result[hash_min_key] < self.myValue ) {
            delete result[hash_min_key];
            result[self.userName] = self.myValue;
          }
        }
      } else {
        result[self.userName] = self.myValue
      }
      var resultJson = JSON.stringify(result);
      self.sendRecords(resultJson)
    }
  }

  function readCollBack(callresult) {
    if (callresult.error != undefined)
      alert(callresult.error);
    else {
      var result = JSON.parse(callresult.result);
      $.each(result, function( index, value ) {
        if($('.records-list').length > 0) {
          $('.records-list').append( "<div class='records-list--items'><span><strong>name</strong> " +
          index +"</span><span><strong>size</strong> " + value +"</span></div>" );
        }
      });
    }
  }

  function saveCollBack(callresult) {
    if (callresult.error != undefined)
      alert(callresult.error);
    else {
    }
  }

  function errorHandler(jqXHR, statusStr, errorStr) {
    alert(statusStr + ' ' + errorStr);
  }
}

